#!/bin/bash
DBNAME="databasename";
DBUSER="databaseuser";
DBPASS="databasepass";
if [[ -z "$1" ]]
  then
    mysql $DBNAME -u $DBUSER -p$DBPASS
  else
    mysql $DBNAME -u $DBUSER -p$DBPASS < $1
fi
