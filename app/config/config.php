<?php
// variabel untuk home base / root directory
DEFINE('BASEURL', 'http://localhost/DirectoryApp');

// nama host database
DEFINE('dbhost', 'localhost');
// nama database
DEFINE('dbname', "namaBasisData");
// namaUserDatabase
DEFINE('dbuser', "userBasisData");
// passwordDatabase
// disarankan : password terdiri dari huruf kecil, huruf kapital, angka dan tanda baca
DEFINE('dbpass', "p@55Word");

DEFINE('rows', 60);
